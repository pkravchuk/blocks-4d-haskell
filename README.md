# Haskell interface to `blocks_4d` and `seed_blocks`

## TODO

 - [x] Write build links 
 - [x] Write fetch config (don't forget to memoize block files)
 - [x] Write up `JJJJ`
 - [x] Run some test bounds
 - [ ] Simplify `Collection/JJJJ.hs` by writing an inspector for block description files
       that will automagically pull out dependencies. Actually we can go further and
       make a TH programm that will create a map from block names to block descriptions
       given a directory of where to look for `.json` files -- this waits unitl future 
       projects because JJJJ is working