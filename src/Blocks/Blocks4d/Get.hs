{-# LANGUAGE ApplicativeDo         #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTSyntax            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}


module Blocks.Blocks4d.Get where

import           Control.Exception                    (Exception, throw)
import           Data.Aeson                           (FromJSON, ToJSON)
import           Data.Binary                          (Binary)
import           Data.Functor.Compose                 (Compose (..))
import qualified Data.Map                             as Map
import           Data.Maybe                           (fromMaybe, isJust)
import           Data.MultiSet                        (empty, fromList)
import           Data.Proxy                           (Proxy (..))
import           Data.Reflection                      (reflect)
import           Data.Rendered                        (mkRendered)
import           Data.Tagged                          (Tagged (..), untag)
import           GHC.Generics                         (Generic)
import qualified Blocks                          as B
import           Blocks.Blocks4d.Description     (AbstractSeedKey (..),
                                                       BlockName (..),
                                                       DescriptionName (..))
import           Blocks.Blocks4d.KnownBlock4d
import           Blocks.Blocks4d.ReadTable.Block (BlockTable4d (..),
                                                       deltaMinusX)
import           Blocks.Blocks4d.Types           (BlockTableKey (..),
                                                       SeedTableKey (..))
import           Blocks.Blocks4d.WriteTable      (renderRational)
import           Blocks.Coordinate               (Coordinate (XT))
import           Blocks.ScalarBlocks.Types       (FourRhoCrossing)
import           Bootstrap.Build.Fetches                   (Fetches, fetch)
import qualified Bootstrap.Math.DampedRational             as DR
import Blocks (DerivMultiplier (..))
import Bootstrap.Math.VectorSpace ((*^))


data Block4dParams = Block4dParams
  { floatFormat   :: Int
  , nmax          :: Int
  , order         :: Int
  , keptPoleOrder :: Int
  , keptPoleOrder_final :: Int
  , precision     :: Int
  }
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Blocks4dGetException b where
  BlockNameNotFound  :: { block :: b, askedName :: String, existingNames :: [String] } -> Blocks4dGetException b
  DerivativeNotFound :: { block :: b, askedDerivative :: (Int, Int), existingDerivatives :: [(Int,Int)] } -> Blocks4dGetException b
  UnsupportedCoordinate :: { block :: b, coordinate :: Coordinate } -> Blocks4dGetException b
  deriving (Show, Exception)

blockToBlockTableKey :: (KnownBlock4d b, Show b)
  => Coordinate -> Block4dParams -> b -> BlockTableKey (DescriptionNameType' b) Int
blockToBlockTableKey coordinate params block = case coordinate of
  XT -> case (params, toAbstractBlockKey block) of
    (Block4dParams {..}, AbstractBlockKey {..}) ->
      let
        mkSeedKey :: AbstractSeedKey -> SeedTableKey ()
        mkSeedKey AbstractSeedKey {..} = SeedTableKey
          { permutation = seedPermutation
          , spin = ()
          , delta12 = mkRendered (renderRational floatFormat) delta12
          , delta34 = mkRendered (renderRational floatFormat) delta34
          , lambda = 2*nmax - 1 + lambdaShift
          , ..
          }
      in
      BlockTableKey
      { description = descriptionName blockName
      , dependencies = maybe [] (map mkSeedKey) (descriptionDeps <$> (descriptionName blockName) <*> pure blockParameters)
      , parameters  = fmap (mkRendered (renderRational floatFormat)) blockParameters
      , spin        = repSpin internalRep
      , lambda      = 2*nmax - 1
      , ..
      }
  _  -> throw (UnsupportedCoordinate block coordinate)

data Block4d b = Block4d { polesToCancel :: [Rational] , unBlock4d :: b }
  deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON)

type instance B.BlockBase (Block4d b) a = FourRhoCrossing a

type instance B.BlockFetchContext (Block4d b) a m = Fetches (BlockTableKey (DescriptionNameType' b) Int) (BlockTable4d a) m
type instance B.BlockTableParams (Block4d b) = Block4dParams

instance (Eq a, RealFloat a, B.KnownCoordinate c, KnownBlock4d b, Show b) => B.IsolatedBlock (Block4d b) (B.Derivative c) a where
  getBlockIsolated = getBlock4dIsolated

instance (Eq a, RealFloat a, B.KnownCoordinate c, KnownBlock4d b, Show b) => B.ContinuumBlock (Block4d b) (B.Derivative c) a where
  getBlockContinuum = getBlock4dContinuum

getBlockTable
  :: forall p m c a b. (B.HasBlocks (Block4d b) p m a, B.KnownCoordinate c, KnownBlock4d b, Show b)
  => Block4d b
  -> Tagged c (Compose (Tagged p) m (BlockTable4d a))
getBlockTable = pure . Compose . pure . fetch .
  blockToBlockTableKey (B.reflectCoordinate @c Proxy) (reflect @p Proxy) . unBlock4d

cancelAllPoles :: (Fractional a, Eq a, Functor f) => [Rational] -> DR.DampedRational base f a -> DR.DampedRational base f a
cancelAllPoles poles dr = foldr unsafeReduce dr (reverse poles)
  where
    unsafeReduce x dr' = case DR.reduceRationalFactor x dr' of
      Just (reg, _) -> reg
      Nothing -> error ("DampedRational does not contain a pole at " ++ show x)

getShiftAndBlockVector
  :: forall b p m a c v.
    (B.HasBlocks (Block4d b) p m a, Functor v, Eq a, Fractional a, B.KnownCoordinate c, KnownBlock4d b, Show b)
  => v (B.Derivative c)
  -> Block4d b
  -> Compose (Tagged p) m (Rational, DR.DampedRational (FourRhoCrossing a) v a)
getShiftAndBlockVector derivVec b'@(Block4d ps b) =
  let
    AbstractBlockKey {..} = toAbstractBlockKey b
  in if isJust (descriptionName blockName) then do
    bt <- untag @c (getBlockTable b')
    pure $ let
        xShift = (fixedDelta internalRep) - deltaMinusX bt
        derivMap = fromMaybe (throw $ BlockNameNotFound b' (show blockName) (Map.keys . blocks $ bt)) $
                    Map.lookup (show blockName) (blocks bt)
        getDeriv d = (*^) (if (toOPEChannel b == TChannel) && (B.crossingOdd d) then -1 else 1) $
                     fromMaybe (throw $ DerivativeNotFound b' (B.unDerivative d) (Map.keys derivMap)) $
                     (Map.lookup (B.unDerivative d) derivMap)
      in
        (xShift,
         cancelAllPoles (map (subtract $ deltaMinusX bt) ps) $
          DR.DampedRational (getDeriv <$> derivVec) (fromList . map (subtract $ deltaMinusX bt) . poles $ bt))
  else pure $ (0, DR.DampedRational (const 0 <$> derivVec) empty)


getBlock4dContinuum
  :: (B.HasBlocks (Block4d b) p m a, Functor v, Eq a, Floating a, B.KnownCoordinate c, KnownBlock4d b, Show b)
  => v (B.Derivative c)
  -> Block4d b
  -> Compose (Tagged p) m (DR.DampedRational (FourRhoCrossing a) v a)
getBlock4dContinuum derivVec b = uncurry DR.shift <$> getShiftAndBlockVector derivVec b

getBlock4dIsolated
  :: (B.HasBlocks (Block4d b) p m a, Functor v, Foldable v, RealFloat a, B.KnownCoordinate c, KnownBlock4d b, Show b)
  => v (B.Derivative c)
  -> Block4d b
  -> Compose (Tagged p) m (v a)
getBlock4dIsolated derivVec b = eval <$> getShiftAndBlockVector derivVec b
  where
    eval (x, v) = DR.evalCancelPoleZeros (fromRational x) v
