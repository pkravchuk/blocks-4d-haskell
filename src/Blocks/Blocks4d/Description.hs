{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}

module Blocks.Blocks4d.Description where

import           Control.Exception          (Exception, IOException, catch,
                                             throw)
import           Data.Binary                (Binary)
import           Data.ByteString            (ByteString)
import qualified Data.ByteString            as BS
import           Data.Kind                  (Type)
import           Data.Map.Strict            (Map)
import           Data.Typeable              (Typeable)
import           GHC.Generics               (Generic)
import           Blocks.Blocks4d.Types (ChiralityType, Permutation)
import           System.FilePath            ((</>))

newtype DescriptionKey = DescriptionKey { key :: String }
  deriving (Show, Generic, Binary)

data DescriptionWriteException n = DescriptionWriteException
  { exceptionDescriptionName :: n
  , orignalException         :: IOException
  }
  deriving (Show, Typeable, Exception)

data AbstractSeedKey = AbstractSeedKey
  { seedPermutation :: Permutation
  , seedType        :: ChiralityType
  , spin_p          :: Int
  , delta12         :: Rational
  , delta34         :: Rational
  , lambdaShift     :: Int
  } deriving (Show, Generic, Binary)

-- We require Show instance of block name to produce the names in .json descriptions
class (DescriptionName (DescriptionNameType n), Show n) => BlockName n where
  type DescriptionNameType n :: Type
  descriptionName :: n -> Maybe (DescriptionNameType n)

class (Binary n, Typeable n, Show n, Ord n) => DescriptionName n where
  descriptionData :: n -> ByteString
  descriptionDeps :: n -> (Map String Rational) -> [AbstractSeedKey]

descriptionFileName :: (DescriptionName n) => FilePath -> n -> FilePath
descriptionFileName baseDir name = baseDir </> ("descripton-" ++ show name ++ ".json")

writeDescription :: (DescriptionName n) => FilePath -> n -> IO ()
writeDescription baseDir descr = catch (BS.writeFile filename (descriptionData descr)) $
    throw . DescriptionWriteException descr
  where
    filename = descriptionFileName baseDir descr
