{-# LANGUAGE MultiWayIf #-}

module Blocks.Blocks4d.ParamCollection where

import Blocks.Blocks4d.Get (Block4dParams (..))

block4dParamsNmax :: Int -> Block4dParams
block4dParamsNmax n = if
  | n <= 6 ->
    Block4dParams
    { nmax          = n
    , keptPoleOrder_final = 8
    , keptPoleOrder = 60
    , order         = 60
    , precision     = 768
    , floatFormat   = 25
    }
  | n <= 10 ->
    Block4dParams
    { nmax          = n
    , keptPoleOrder_final = 14
    , keptPoleOrder = 60
    , order         = 60
    , precision     = 768
    , floatFormat   = 25
    }
  | n <= 12 ->
    Block4dParams
    { nmax          = n
    , keptPoleOrder_final = 18
    , keptPoleOrder = 60
    , order         = 60
    , precision     = 768
    , floatFormat   = 25
    }
  | n <= 14 ->
    Block4dParams
    { nmax          = n
    , keptPoleOrder_final = 20
    , keptPoleOrder = 60
    , order         = 60
    , precision     = 768
    , floatFormat   = 25
    }
  | n <= 18 ->
    Block4dParams
    { nmax          = 18
    , keptPoleOrder_final = 32
    , keptPoleOrder = 80
    , order         = 80
    , precision     = 960
    , floatFormat   = 25
    }
  | n <= 22 ->
    Block4dParams
    { nmax          = 22
    , keptPoleOrder_final = 40
    , keptPoleOrder = 90
    , order         = 90
    , precision     = 1024
    , floatFormat   = 25
    }
  | True -> error ("No preset block table parameters for nmax " ++ show n)