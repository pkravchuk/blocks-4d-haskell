{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveDataTypeable    #-}
{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE OverloadedRecordDot   #-}

module Blocks.Blocks4d.Types where

import           Data.Aeson      (FromJSON (..), ToJSON (..))
import           Data.Binary     (Binary)
import qualified Data.Map.Strict as Map
import           Data.Rendered   (Rendered)
import           Data.Typeable   (Typeable)
import           GHC.Generics    (Generic)
import           Hyperion        (Dict (..), Static (..), cAp, closureDict,
                                  ptrAp)


data Permutation = Perm1234 | Perm1243
  deriving (Eq, Ord, Enum, Bounded, Generic, Binary)

data ChiralityType = Primal | Dual
  deriving (Eq, Ord, Enum, Bounded, Generic, Binary)

instance Show Permutation where
  show Perm1234 = "1234"
  show Perm1243 = "1243"

instance FromJSON Permutation where
  parseJSON x = parseJSON x >>= \s -> case s of "1234" -> return Perm1234
                                                "1243" -> return Perm1243
                                                _      -> fail $ "Not a valid permutation: " ++ s

instance ToJSON Permutation where
  toJSON = toJSON . show

instance Show ChiralityType where
  show Primal = "primal"
  show Dual   = "dual"

instance FromJSON ChiralityType where
  parseJSON x = parseJSON x >>= \s -> case s of "primal" -> return Primal
                                                "dual"   -> return Dual
                                                _        -> fail $ "Not a valid seed type: " ++ s
instance ToJSON ChiralityType where
  toJSON = toJSON . show

data SeedTableKey j = SeedTableKey
  { permutation   :: Permutation
  , seedType      :: ChiralityType
  , spin_p        :: Int
  , spin          :: j
  , delta12       :: Rendered Rational
  , delta34       :: Rendered Rational
  , lambda        :: Int
  , order         :: Int
  , keptPoleOrder :: Int
  , precision     :: Int
  }
  deriving (Eq, Ord, Show, Generic, Typeable, Binary, FromJSON, ToJSON, Functor)

instance (Typeable j, Static (Binary j)) => Static (Binary (SeedTableKey j)) where
  closureDict = static (\Dict -> Dict) `ptrAp` closureDict @(Binary j)

instance Semigroup s => Semigroup (SeedTableKey s) where
  a <> b = (a :: SeedTableKey s) { spin = a.spin <> b.spin }

data BlockTableKey n j = BlockTableKey
  { description         :: Maybe n -- 'Nothing' indicates that the block is actually 0
  , dependencies        :: [SeedTableKey ()]
  , parameters          :: Map.Map String (Rendered Rational)
  , floatFormat         :: Int
  , spin                :: j
  , lambda              :: Int
  , order               :: Int
  , keptPoleOrder       :: Int
  , keptPoleOrder_final :: Int
  , precision           :: Int
  }
  deriving (Eq, Ord, Show, Generic, Typeable, Binary, FromJSON, ToJSON, Functor)

instance (Typeable j, Static (Binary j), Typeable n, Static (Binary n)) => Static (Binary (BlockTableKey n j)) where
  closureDict = static (\Dict Dict -> Dict) `ptrAp` closureDict @(Binary j) `cAp` closureDict @(Binary n)


instance Semigroup s => Semigroup (BlockTableKey n s) where
  a <> b = (a :: BlockTableKey n s) { spin = a.spin <> b.spin }
