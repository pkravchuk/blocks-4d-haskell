{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE StaticPointers    #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE TypeFamilies      #-}

module Blocks.Blocks4d.Collection.JJJJ.Descriptions where

import           Data.Binary                       (Binary)
import           Data.FileEmbed                    (embedFile,
                                                    makeRelativeToProject)
import           Data.Map.Strict                   (Map)
import           GHC.Generics                      (Generic)
import Hyperion ( Static(..), Dict(..), cPtr )
import           Blocks.Blocks4d.Description  (AbstractSeedKey (..),
                                                    BlockName (..),
                                                    DescriptionName (..))
import           Blocks.Blocks4d.KnownBlock4d ()
import           Blocks.Blocks4d.Types        (ChiralityType (..),
                                                    Permutation (..))
import           Blocks.Blocks4d.Util         (Parity (..), parity)

data JJJJBlockName =
  JJJJBlock Int (Either Parity Int) Int Int Int
  deriving (Generic, Binary, Eq, Ord)

instance Show JJJJBlockName where
  show (JJJJBlock p (Left par) a b s) = "blockP" ++ show p ++ "Spin" ++ show par ++
    (if p == 0 && par == Even then show (min a b) ++ show (max a b) else "") ++
    "Structure" ++ show s
  show (JJJJBlock p (Right spin) _ _ s) = "blockP" ++ show p ++ "Spin" ++ show spin ++
    "Structure" ++ show s

toBlockName :: Int -> Int -> Int -> Int -> Int -> JJJJBlockName
toBlockName l p a b s = if p == 0 && l == 0 then JJJJBlock p (Right 0) a b s
                                            else JJJJBlock p (Left (parity l)) a b s

data JJJJDescriptionName =
  JJJJDescription Int (Either Parity Int) Int
  deriving (Generic, Binary, Eq, Ord)
-- deriveDescriptionNameStatics ''JJJJDescriptionName
instance Static (Binary JJJJDescriptionName) where
          closureDict = cPtr (static Dict)
instance Static (DescriptionName JJJJDescriptionName) where
          closureDict = cPtr (static Dict)

instance Show JJJJDescriptionName where
  show (JJJJDescription p (Left par) s) = "blockP" ++ show p ++ "Spin" ++ show par ++
    "Structure" ++ show s
  show (JJJJDescription p (Right spin) s) = "blockP" ++ show p ++ "Spin" ++ show spin ++
    "Structure" ++ show s


seedKeyJJJJ :: (Rational, Rational) -> Int -> Int -> (Map String Rational) -> AbstractSeedKey
seedKeyJJJJ (ap,bp) lambdaShift spin_p _ = AbstractSeedKey
  { seedPermutation = Perm1234
  , seedType = Primal
  , delta12 = -2*ap
  , delta34 = 2*bp
  , ..
  }

instance BlockName JJJJBlockName where
  type DescriptionNameType JJJJBlockName = JJJJDescriptionName
  descriptionName (JJJJBlock p lp _ _ s) = if p == 0 && lp == Left Odd && (s == 2 || s == 5) then Nothing
                                           else Just (JJJJDescription p lp s)

instance DescriptionName JJJJDescriptionName where
  descriptionData (JJJJDescription 0 (Right 0) 1) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0Spin0Structure1.json" >>= embedFile)
  descriptionData (JJJJDescription 0 (Right 0) 2) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0Spin0Structure2.json" >>= embedFile)
  descriptionData (JJJJDescription 0 (Right 0) 3) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0Spin0Structure3.json" >>= embedFile)
  descriptionData (JJJJDescription 0 (Right 0) 4) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0Spin0Structure4.json" >>= embedFile)
  descriptionData (JJJJDescription 0 (Right 0) 5) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0Spin0Structure5.json" >>= embedFile)
  descriptionData (JJJJDescription 0 (Right 0) 6) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0Spin0Structure6.json" >>= embedFile)
  descriptionData (JJJJDescription 0 (Right 0) 7) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0Spin0Structure7.json" >>= embedFile)

  descriptionData (JJJJDescription 0 (Left Even) 1) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinEvenStructure1.json" >>= embedFile)
  descriptionData (JJJJDescription 0 (Left Even) 2) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinEvenStructure2.json" >>= embedFile)
  descriptionData (JJJJDescription 0 (Left Even) 3) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinEvenStructure3.json" >>= embedFile)
  descriptionData (JJJJDescription 0 (Left Even) 4) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinEvenStructure4.json" >>= embedFile)
  descriptionData (JJJJDescription 0 (Left Even) 5) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinEvenStructure5.json" >>= embedFile)
  descriptionData (JJJJDescription 0 (Left Even) 6) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinEvenStructure6.json" >>= embedFile)
  descriptionData (JJJJDescription 0 (Left Even) 7) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinEvenStructure7.json" >>= embedFile)

  descriptionData (JJJJDescription 0 (Left Odd) 1) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinOddStructure1.json" >>= embedFile)
--  descriptionData (JJJJDescription 0 (Left Odd) 2) =
--    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinOddStructure2.json" >>= embedFile)
  descriptionData (JJJJDescription 0 (Left Odd) 3) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinOddStructure3.json" >>= embedFile)
  descriptionData (JJJJDescription 0 (Left Odd) 4) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinOddStructure4.json" >>= embedFile)
--  descriptionData (JJJJDescription 0 (Left Odd) 5) =
--    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinOddStructure5.json" >>= embedFile)
  descriptionData (JJJJDescription 0 (Left Odd) 6) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinOddStructure6.json" >>= embedFile)
  descriptionData (JJJJDescription 0 (Left Odd) 7) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinOddStructure7.json" >>= embedFile)

  descriptionData (JJJJDescription 2 (Left Even) 1) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinEvenStructure1.json" >>= embedFile)
  descriptionData (JJJJDescription 2 (Left Even) 2) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinEvenStructure2.json" >>= embedFile)
  descriptionData (JJJJDescription 2 (Left Even) 3) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinEvenStructure3.json" >>= embedFile)
  descriptionData (JJJJDescription 2 (Left Even) 4) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinEvenStructure4.json" >>= embedFile)
  descriptionData (JJJJDescription 2 (Left Even) 5) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinEvenStructure5.json" >>= embedFile)
  descriptionData (JJJJDescription 2 (Left Even) 6) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinEvenStructure6.json" >>= embedFile)
  descriptionData (JJJJDescription 2 (Left Even) 7) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinEvenStructure7.json" >>= embedFile)

  descriptionData (JJJJDescription 2 (Left Odd) 1) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinOddStructure1.json" >>= embedFile)
  descriptionData (JJJJDescription 2 (Left Odd) 2) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinOddStructure2.json" >>= embedFile)
  descriptionData (JJJJDescription 2 (Left Odd) 3) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinOddStructure3.json" >>= embedFile)
  descriptionData (JJJJDescription 2 (Left Odd) 4) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinOddStructure4.json" >>= embedFile)
  descriptionData (JJJJDescription 2 (Left Odd) 5) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinOddStructure5.json" >>= embedFile)
  descriptionData (JJJJDescription 2 (Left Odd) 6) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinOddStructure6.json" >>= embedFile)
  descriptionData (JJJJDescription 2 (Left Odd) 7) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinOddStructure7.json" >>= embedFile)

  descriptionData (JJJJDescription 4 (Left Even) 1) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=4/blockP4SpinEvenStructure1.json" >>= embedFile)
  descriptionData (JJJJDescription 4 (Left Even) 2) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=4/blockP4SpinEvenStructure2.json" >>= embedFile)
  descriptionData (JJJJDescription 4 (Left Even) 3) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=4/blockP4SpinEvenStructure3.json" >>= embedFile)
  descriptionData (JJJJDescription 4 (Left Even) 4) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=4/blockP4SpinEvenStructure4.json" >>= embedFile)
  descriptionData (JJJJDescription 4 (Left Even) 5) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=4/blockP4SpinEvenStructure5.json" >>= embedFile)
  descriptionData (JJJJDescription 4 (Left Even) 6) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=4/blockP4SpinEvenStructure6.json" >>= embedFile)
  descriptionData (JJJJDescription 4 (Left Even) 7) =
    $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=4/blockP4SpinEvenStructure7.json" >>= embedFile)

  descriptionData _ = error "Unknown JJJJ description name"

  descriptionDeps (JJJJDescription 0 (Right 0) _) = sequence [seedKeyJJJJ (0,0) 8 0]

  descriptionDeps (JJJJDescription 0 (Left Even) _) = sequence[ seedKeyJJJJ (0,0) 8 0
                                                              , seedKeyJJJJ (1,-1) 8 0
                                                              , seedKeyJJJJ (1,0) 8 0
                                                              , seedKeyJJJJ (1,1) 8 0]
  descriptionDeps (JJJJDescription 0 (Left Odd) _) = sequence [seedKeyJJJJ (0,0) 8 0]

  descriptionDeps (JJJJDescription 2 (Left Even) _) = sequence [ seedKeyJJJJ (1/2,-1/2) 8 2 ]
  descriptionDeps (JJJJDescription 2 (Left Odd) _) = sequence [ seedKeyJJJJ (1/2,-1/2) 8 2 ]

  descriptionDeps (JJJJDescription 4 (Left Even) _) = sequence [ seedKeyJJJJ (1,-1) 8 4]
  descriptionDeps _ = error "Unknown JJJJ description name"

-- {Htt[{0, 0}, {d, l}, {0, 0}]}
---- lambdaShift == 8
--blockP0Spin0Descriptions :: [Maybe (Description AbstractSeedKey)]
--blockP0Spin0Descriptions = Just <$>
--  [ Description (DescriptionKey "blockP0Spin0Structure1")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0Spin0Structure1.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP0Spin0Structure2")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0Spin0Structure2.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP0Spin0Structure3")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0Spin0Structure3.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP0Spin0Structure4")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0Spin0Structure4.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP0Spin0Structure5")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0Spin0Structure5.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP0Spin0Structure6")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0Spin0Structure6.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP0Spin0Structure7")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0Spin0Structure7.json" >>= embedFile)
--                deps
--  ]
--  where deps = sequence [seedKeyJJJJ (0,0) 8 0]
--
---- {Htt[{0, 0}, {d, l}, {0, 0}], Htt[{0, 0}, {d, l}, {1, -1}],
---- Htt[{0, 0}, {d, l}, {1, 0}], Htt[{0, 0}, {d, l}, {1, 1}]}
---- lambdaShift == 8
--blockP0SpinEvenDescriptions :: [Maybe (Description AbstractSeedKey)]
--blockP0SpinEvenDescriptions = Just <$>
--  [ Description (DescriptionKey "blockP0SpinEvenStructure1")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinEvenStructure1.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP0SpinEvenStructure2")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinEvenStructure2.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP0SpinEvenStructure3")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinEvenStructure3.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP0SpinEvenStructure4")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinEvenStructure4.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP0SpinEvenStructure5")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinEvenStructure5.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP0SpinEvenStructure6")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinEvenStructure6.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP0SpinEvenStructure7")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinEvenStructure7.json" >>= embedFile)
--                deps
--  ]
--  where deps = sequence [ seedKeyJJJJ (0,0) 8 0
--                        , seedKeyJJJJ (1,-1) 8 0
--                        , seedKeyJJJJ (1,0) 8 0
--                        , seedKeyJJJJ (1,1) 8 0]
--
---- {Htt[{0, 0}, {d, l}, {0, 0}]}
---- lambdaShift == 8
--blockP0SpinOddDescriptions :: [Maybe (Description AbstractSeedKey)]
--blockP0SpinOddDescriptions =
--  [ Just $ Description (DescriptionKey "blockP0SpinOddStructure1")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinOddStructure1.json" >>= embedFile)
--                deps
--  , Nothing
--  , Just $ Description (DescriptionKey "blockP0SpinOddStructure3")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinOddStructure3.json" >>= embedFile)
--                deps
--  , Just $ Description (DescriptionKey "blockP0SpinOddStructure4")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinOddStructure4.json" >>= embedFile)
--                deps
--  , Nothing
--  , Just $ Description (DescriptionKey "blockP0SpinOddStructure6")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinOddStructure6.json" >>= embedFile)
--                deps
--  , Just $ Description (DescriptionKey "blockP0SpinOddStructure7")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=0/blockP0SpinOddStructure7.json" >>= embedFile)
--                deps
--  ]
--  where deps = sequence [seedKeyJJJJ (0,0) 8 0]
--
---- {Htt[{2, 0}, {d, l}, {1/2, -(1/2)}],
---- Htt[{2, 1}, {d, l}, {1/2, -(1/2)}],
---- Htt[{2, 2}, {d, l}, {1/2, -(1/2)}]}
---- lambdaShift = 8
--blockP2SpinEvenDescriptions :: [Maybe (Description AbstractSeedKey)]
--blockP2SpinEvenDescriptions = Just <$>
--  [ Description (DescriptionKey "blockP2SpinEvenStructure1")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinEvenStructure1.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP2SpinEvenStructure2")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinEvenStructure2.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP2SpinEvenStructure3")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinEvenStructure3.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP2SpinEvenStructure4")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinEvenStructure4.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP2SpinEvenStructure5")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinEvenStructure5.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP2SpinEvenStructure6")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinEvenStructure6.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP2SpinEvenStructure7")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinEvenStructure7.json" >>= embedFile)
--                deps
--  ]
--  where deps = sequence [ seedKeyJJJJ (1/2,-1/2) 8 2 ]
--
---- {Htt[{2, 0}, {d, l}, {1/2, -(1/2)}],
---- Htt[{2, 1}, {d, l}, {1/2, -(1/2)}],
---- Htt[{2, 2}, {d, l}, {1/2, -(1/2)}]}
---- lambdaShift = 8
--blockP2SpinOddDescriptions :: [Maybe (Description AbstractSeedKey)]
--blockP2SpinOddDescriptions = Just <$>
--  [ Description (DescriptionKey "blockP2SpinOddStructure1")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinOddStructure1.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP2SpinOddStructure2")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinOddStructure2.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP2SpinOddStructure3")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinOddStructure3.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP2SpinOddStructure4")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinOddStructure4.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP2SpinOddStructure5")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinOddStructure5.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP2SpinOddStructure6")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinOddStructure6.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP2SpinOddStructure7")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=2/blockP2SpinOddStructure7.json" >>= embedFile)
--                deps
--  ]
--  where deps = sequence [ seedKeyJJJJ (1/2,-1/2) 8 2 ]
--
---- {Htt[{4, 0}, {d, l}, {1, -1}], Htt[{4, 1}, {d, l}, {1, -1}],
---- Htt[{4, 2}, {d, l}, {1, -1}], Htt[{4, 3}, {d, l}, {1, -1}],
---- Htt[{4, 4}, {d, l}, {1, -1}]}
---- lambdaShift = 8
--blockP4SpinEvenDescriptions :: [Maybe (Description AbstractSeedKey)]
--blockP4SpinEvenDescriptions = Just <$>
--  [ Description (DescriptionKey "blockP4SpinEvenStructure1")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=4/blockP4SpinEvenStructure1.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP4SpinEvenStructure2")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=4/blockP4SpinEvenStructure2.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP4SpinEvenStructure3")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=4/blockP4SpinEvenStructure3.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP4SpinEvenStructure4")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=4/blockP4SpinEvenStructure4.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP4SpinEvenStructure5")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=4/blockP4SpinEvenStructure5.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP4SpinEvenStructure6")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=4/blockP4SpinEvenStructure6.json" >>= embedFile)
--                deps
--  , Description (DescriptionKey "blockP4SpinEvenStructure7")
--                $(makeRelativeToProject "src/Blocks/Blocks4d/Collection/JJJJ/p=4/blockP4SpinEvenStructure7.json" >>= embedFile)
--                deps
--  ]
--  where deps = sequence [ seedKeyJJJJ (1,-1) 8 4]


--
--blockP0Spin0Names :: Int -> String
--blockP0Spin0Names = ("blockP0Spin0Structure" ++) . show
--
--blockP0SpinEvenNames :: Int -> Int -> Int -> String
--blockP0SpinEvenNames a b s = "blockP0SpinEven" ++ show (min a b) ++ show (max a b) ++ "Structure" ++ show s
--
--blockP0SpinOddNames :: Int -> String
--blockP0SpinOddNames = ("blockP0SpinOddStructure" ++) . show
--
--blockP2SpinEvenNames :: Int -> String
--blockP2SpinEvenNames = ("blockP2SpinEvenStructure" ++) . show
--
--blockP2SpinOddNames :: Int -> String
--blockP2SpinOddNames = ("blockP2SpinOddStructure" ++) . show
--
--blockP4SpinEvenNames :: Int -> String
--blockP4SpinEvenNames = ("blockP4SpinEvenStructure" ++) . show

--
--blockP0SpinOddDescriptions :: [ByteString]
--blockP0SpinOddDescriptions = undefined
--
--blockP2SpinEvenDescriptions :: [ByteString]
--blockP2SpinEvenDescriptions = undefined
--
--blockP2SpinOddDescriptions :: [ByteString]
--blockP2SpinOddDescriptions = undefined
--
--blockP4SpinEvenDescriptions :: [ByteString]
--blockP4SpinEvenDescriptions = undefined
