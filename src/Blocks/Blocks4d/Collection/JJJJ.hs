{-# LANGUAGE ConstraintKinds       #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE InstanceSigs          #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}

module Blocks.Blocks4d.Collection.JJJJ where

import           Control.Exception                                 (Exception,
                                                                    throw)
import           Data.Aeson                                        (FromJSON,
                                                                    ToJSON)
import           Data.Binary                                       (Binary)
import           Data.Foldable                                     (toList)
import qualified Data.Foldable                                     as Foldable
import           Data.Functor.Compose                              (Compose)
import qualified Data.Map                                          as Map
import           Data.Map.Strict                                   (empty)
import           Data.Proxy                                        (Proxy (..))
import           Data.Reflection                                   (reflect)
import qualified Data.Set                                          as Set
import           Data.Tagged                                       (Tagged)
import           GHC.Generics                                      (Generic)
import           Blocks                                       (Block (..), BlockFetchContext,
                                                                    BlockTableParams,
                                                                    Coordinate (..),
                                                                    DerivMultiplier (..),
                                                                    Derivative (..),
                                                                    HasBlocks,
                                                                    IsolatedBlock (..),
                                                                    KnownCoordinate (..),
                                                                    getDeriv,
                                                                    yEven,
                                                                    zzbDerivsAll)
import           Blocks.Blocks4d.Collection.JJJJ.Descriptions
import           Blocks.Blocks4d.Get                          (Block4dParams (..))
import           Blocks.Blocks4d.KnownBlock4d                 (AbstractBlockKey (..),
                                                                    HasUnitarity (..),
                                                                    KnownBlock4d (..))
import           Blocks.Delta                                 (Delta (..))
import           Bootstrap.Math.Util                                    (pochhammer)


data JJJJBlockException =
  MismatchedInternalReps JJJJThreePointStructure JJJJThreePointStructure
  | ImpossibleRepresentation JJJJInternalRep
  | MalformedJJJJBlock JJJJBlock
  deriving (Show, Exception)

data JJJJInternalRep = JJJJInternalRep { l :: Int, p :: Int, delta :: Delta }
  deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON)

fixedDelta_ :: Delta -> Rational -> Rational
fixedDelta_ delta unitarity = case delta of
  Fixed d             -> d
  RelativeUnitarity x -> unitarity + x

instance HasUnitarity JJJJInternalRep where
  unitarityBound JJJJInternalRep {..} | l == 0 = 1 + fromIntegral p/2
  unitarityBound JJJJInternalRep {..} | l > 0 = 2 + fromIntegral p/2 + fromIntegral l
  unitarityBound r = throw $ ImpossibleRepresentation r
  fixedDelta r@(JJJJInternalRep {..}) = fixedDelta_ delta (unitarityBound r)
  repSpin = l

data JJJJThreePointStructure = JJJJThreePointStructure JJJJInternalRep Int
  deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON)

data JJJJFourPointFunctional = JJJJFourPointFunctional Int DerivMultiplier
  deriving (Eq, Ord, Show, Generic, Binary)

type JJJJBlock = Block JJJJThreePointStructure JJJJFourPointFunctional

internalRepresentation :: JJJJBlock -> JJJJInternalRep
internalRepresentation (Block s1@(JJJJThreePointStructure r1 _) s2@(JJJJThreePointStructure r2 _) _) =
  if r1 == r2 then
    r1
  else
    throw (MismatchedInternalReps s1 s2)

instance KnownBlock4d JJJJBlock where
  type InternalRep JJJJBlock = JJJJInternalRep
  type BlockNameType JJJJBlock = JJJJBlockName
  type DescriptionNameType' JJJJBlock = JJJJDescriptionName
  toAbstractBlockKey block@(Block sl sr fn) = AbstractBlockKey
    { blockParameters = empty
    , internalRep = internalRepresentation block
    , blockName = name
    }
    where
      name = case (sl, sr, fn) of
        (JJJJThreePointStructure (JJJJInternalRep l p _) a
          , JJJJThreePointStructure _ b
          , JJJJFourPointFunctional s _) -> toBlockName l p a b s
  toOPEChannel (Block _ _ (JJJJFourPointFunctional _ c)) = c


newtype JJJJIdentityBlock = JJJJIdentityBlock { unJJJJIdentityBlock :: Block () JJJJFourPointFunctional }
  deriving (Eq, Ord, Show, Generic, Binary)

type instance BlockFetchContext JJJJIdentityBlock a m = ()
type instance BlockTableParams JJJJIdentityBlock = Block4dParams

instance (Eq a, RealFloat a, KnownCoordinate c) => IsolatedBlock JJJJIdentityBlock (Derivative c) a where
  getBlockIsolated
      :: forall p m v. (HasBlocks JJJJIdentityBlock p m a, Applicative v, Foldable v)
      => v (Derivative c)
      -> JJJJIdentityBlock
      -> Compose (Tagged p) m (v a)
  getBlockIsolated derivs block =
    pure $ fmap (getDeriv yEven derivativeMap) derivs
    where
      derivativeMap = fromZZb yEven nmax' (getIdentityBlockZZB nmax' block)
      nmax' = nmax $ reflect @p Proxy
      -- This hack, suggested here
      -- (https://ghc.readthedocs.io/en/8.0.1/using-warnings.html#ghc-flag--Wredundant-constraints)
      -- suppresses the redundant constraint warning for Foldable
      _ = Foldable.length derivs

getIdentityBlockZZB
  :: forall a. Floating a => Int -> JJJJIdentityBlock -> Map.Map (Derivative 'ZZb) a
getIdentityBlockZZB nmax (JJJJIdentityBlock (Block () () (JJJJFourPointFunctional i c)))
  = Map.fromSet compute (Set.fromList (toList (zzbDerivsAll nmax)))
  where
    compute (Derivative (m,n)) = (compute' (m,n) + compute' (n,m)) * case c of
      SChannel -> 1
      TChannel -> (-1)^(m+n)
    compute' (m,n) = case i of
      1 -> (-1)^(m+n) * 2^(m+2+n+4) * pochhammer 2 m * pochhammer 4 n / 16
      2 -> - (-1)^(m+n) * 2^(m+2+n+4) * pochhammer 2 m * pochhammer 4 n / 4
      6 -> (-1)^(m+n) * 2^(m+3+n+3) * pochhammer 3 m * pochhammer 3 n / 8
      7 -> (-1)^(m+n) * 2^(m+3+n+3) * pochhammer 3 m * pochhammer 3 n / 8
      _ -> 0
