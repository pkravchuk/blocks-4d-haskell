{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE RecordWildCards        #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}

module Blocks.Blocks4d.KnownBlock4d where

import           Data.Kind                        (Type)
import           Data.Map.Strict                  (Map)
import           Data.Typeable                    (Typeable)
import           Blocks.Blocks4d.Description (BlockName (..))
import           Blocks (DerivMultiplier)

class HasUnitarity r where
  unitarityBound :: r -> Rational
  fixedDelta     :: r -> Rational
  repSpin        :: r -> Int

-- | To simplify the build process and allow better parallelism,
-- 'blockDescription' should only export one block (i.e. one scalar function)
data AbstractBlockKey r n = AbstractBlockKey
    { blockName       :: n
    , blockParameters :: Map String Rational
    , internalRep     :: r
    }

class (Typeable b, HasUnitarity (InternalRep b)
      , BlockName (BlockNameType b)
      , DescriptionNameType (BlockNameType b) ~ DescriptionNameType' b)
      => KnownBlock4d b where
  type InternalRep b :: Type
  type BlockNameType b  :: Type
  type DescriptionNameType' b :: Type
  toAbstractBlockKey :: b -> AbstractBlockKey (InternalRep b) (BlockNameType b)
  toOPEChannel :: b -> DerivMultiplier
