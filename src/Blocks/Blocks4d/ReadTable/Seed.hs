{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Blocks.Blocks4d.ReadTable.Seed where


import           Control.Applicative                ((<|>))
import           Data.Attoparsec.ByteString.Char8   (Parser)
import qualified Data.Attoparsec.ByteString.Char8   as A
import qualified Data.ByteString.Char8              as BS
import           Data.Functor.Identity              (Identity (..))
import           Data.Map.Strict                    (Map)
import qualified Data.Map.Strict                    as Map
import qualified Data.MultiSet                      as MultiSet
import           Data.Proxy                         (Proxy (..))
import           Data.Reflection                    (reflect)
import           Blocks.Blocks4d.Types         (ChiralityType (..),
                                                     SeedTableKey (..))
import           Blocks.Blocks4d.WriteTable    (seedTableFilePath)
import           Blocks.ScalarBlocks.ReadTable (cbPoles, ignore,
                                                     polynomial, spaced)
import           Blocks.ScalarBlocks.Types     (FourRhoCrossing)
import qualified Bootstrap.Math.DampedRational           as DR
import           Bootstrap.Math.Polynomial               (Polynomial)
import qualified Bootstrap.Math.Polynomial               as Pol

type SeedDerivMap = Map (Int, Int, Int)

derivMap :: RealFloat a => Parser (SeedDerivMap (Polynomial a))
derivMap = ignore comment *> "{" *> fmap Map.fromList (mapPair `A.sepBy` spaced ",") <* "}"
  where
    comment = spaced ("(*" *> A.manyTill A.anyChar "*)")
    deriv = (,,) <$> (("seedBlockE" *> A.decimal)  <|> ("xtDeriv" *> pure 0))
                 <*> ("[" *> A.decimal <* spaced ",") <*> A.decimal <* "]"
    mapPair = (,) <$> (deriv <* spaced "->") <*> polynomial

readDerivMapFile :: RealFloat a => FilePath -> IO (Either String (SeedDerivMap (Polynomial a)))
readDerivMapFile file = A.parseOnly derivMap <$> BS.readFile file

seedRecursionPolesSingle :: ChiralityType -> Int -> Int -> Rational -> [Rational]
seedRecursionPolesSingle Primal p 0 shift = map (subtract shift) [(2+fromIntegral p)/2]
seedRecursionPolesSingle Primal p l shift = map (subtract shift) [(2+fromIntegral p)/2
                                                                 , (4+2*fromIntegral l+fromIntegral p)/2]
seedRecursionPolesSingle Dual   _ 0 _ = []
seedRecursionPolesSingle Dual   p _ shift = map (subtract shift) [-(fromIntegral p-4)/2]

seedRecursionPolesAll :: ChiralityType -> Int -> Int -> [Rational]
seedRecursionPolesAll t p l = concat [ seedRecursionPolesSingle t (p-n+1) l (s*(fromIntegral n-1)/2) | n <- [1..p] ]
  where
    s = case t of Primal -> 1
                  Dual   -> -1

seedRecursionCoefficient :: ChiralityType -> Int -> Int -> Rational
seedRecursionCoefficient Primal p l = 1/(256 * fromIntegral (l+p))
seedRecursionCoefficient Dual   p l = 1/(128 * fromIntegral (l+p))

seedCoefficient :: ChiralityType -> Int -> Int -> Rational
seedCoefficient t p l = product [ seedRecursionCoefficient t (p-n+1) l | n <- [1..p] ]

seedPoles :: ChiralityType -> Int -> Int -> Int -> [Rational]
seedPoles t p l kPO   =
    map (subtract shift) scalarPoles ++ seedRecursionPolesAll t p l
  where
    scalarPoles = cbPoles 1 l kPO
    shift = case t of Primal -> fromIntegral p/2
                      Dual   -> -fromIntegral p/2

cbPrefactor
  :: forall a. (Floating a, Eq a)
  => ChiralityType
  -> Int
  -> Int
  -> Int
  -> DR.DampedRational (FourRhoCrossing a) Identity a
cbPrefactor t p l kPO =
  DR.shift (fromIntegral l + 2 + fromIntegral p/2) $
  DR.DampedRational (Identity coeff) (MultiSet.fromList poles)
  where
    coeff = Pol.constant $
      fromRational (seedCoefficient t p l) *
      (reflect (Proxy :: Proxy (FourRhoCrossing a))) ** pwr
    pwr = case t of
      Primal -> fromIntegral p / 2
      Dual   -> - fromIntegral p / 2
    poles = seedPoles t p l kPO

readSeedBlockTable
  :: RealFloat a
  => FilePath
  -> SeedTableKey Int
  -> IO (DR.DampedRational (FourRhoCrossing a) SeedDerivMap a)
readSeedBlockTable blockTableDir b@SeedTableKey{..} = do
  m <- fmap (either error id) (readDerivMapFile (seedTableFilePath blockTableDir b))
  return (DR.mapNumerator (\(Identity n) -> fmap (n*) m) $
           cbPrefactor seedType spin_p spin keptPoleOrder)

