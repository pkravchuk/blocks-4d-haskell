{-# LANGUAGE DeriveAnyClass             #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE ScopedTypeVariables        #-}

module Blocks.Blocks4d.ReadTable.Block where

import           Control.Exception                (Exception, throw)
import           Control.Monad                    (MonadPlus, mzero)
import           Data.Aeson                       (FromJSON (..), (.:))
import qualified Data.Aeson                       as Aeson
import qualified Data.Map.Strict                  as Map
import qualified Data.Text                        as Text
import qualified Data.Text.Read                   as Text
import           Blocks.Blocks4d.Description (DescriptionName)
import           Blocks.Blocks4d.Types       (BlockTableKey,
                                                   ChiralityType (..))
import           Blocks.Blocks4d.WriteTable  (blockTableFilePath)
import           Bootstrap.Math.Polynomial             (Polynomial (..),
                                                   mapCoefficients)

-- Below is copied from Blocks.Blocks3d.ReadTable since these are not exported
-- perhaps should remove/extend the export list over there and just import this here
-- Or better yet split into a util module?

textToFractional :: (MonadPlus m, Fractional a) => Text.Text -> m a
textToFractional t = case Text.rational t of
  Right (x, "") -> return x
  _             -> mzero

newtype QuotedFractional a = QuotedFractional { unQuoted :: a }
  deriving newtype (Eq, Num)

instance Fractional a => FromJSON (QuotedFractional a) where
  parseJSON = Aeson.withText "QuotedFractional" (fmap QuotedFractional . textToFractional)

newtype PolynomialJSON a = PolynomialJSON { toPolynomial :: Polynomial a }

instance (Fractional a, Eq a) => FromJSON (PolynomialJSON a) where
  parseJSON = fmap (PolynomialJSON . mapCoefficients unQuoted) . parseJSON

unNestVectors2 :: [[a]] -> Map.Map (Int,Int) a
unNestVectors2 vecs = Map.fromList $ do
  (i, vecs_i)  <- zip [0..] vecs
  (j, vecs_ij) <- zip [0..] vecs_i
  pure ((i,j), vecs_ij)

-- End of Blocks.Blocks3d.ReadTable copy-paste

data BlockTable4d a = BlockTable4d
  { exchangeP           :: Int
  , exchangeType        :: ChiralityType
  , exchangeSpin        :: Int
  , lambda              :: Int
  , order               :: Int
  , keptPoleOrder       :: Int
  , keptPoleOrder_final :: Int
  , poles               :: [Rational]
  , parameters          :: Map.Map String a
  , deltaMinusX         :: Rational
  , blocks              :: Map.Map String (Map.Map (Int,Int) (Polynomial a))
  }

-- We are ignoring the "description" field in this instance
-- Should probably get rid of it in blocks_4d
instance (Fractional a, Eq a) => FromJSON (BlockTable4d a) where
  parseJSON = Aeson.withObject "BlockTable4d" $ \obj -> do
    exchangeP     <- obj .: "p"
    exchangeType  <- obj .: "type"
    exchangeSpin  <- obj .: "spin"
    lambda        <- obj .: "lambda"
    order         <- obj .: "order"
    keptPoleOrder <- obj .: "kept_pole_order"
    keptPoleOrder_final <- obj .: "final_kept_pole_order"
    quotedPoles   <- obj .: "poles"
    quotedParams  <- obj .: "parameters"
    quotedDMX     <- obj .: "delta_minus_x"
    blockMapRaw   <- obj .: "blocks"
    let
      poles       = map unQuoted quotedPoles
      parameters  = fmap unQuoted quotedParams
      blocks      = unNestVectors2 <$> fmap (fmap (fmap toPolynomial)) blockMapRaw
      deltaMinusX = unQuoted quotedDMX
    return BlockTable4d {..}

data ReadBlockTableException n = ReadBlockTableException
  { exceptionKey  :: BlockTableKey n Int
  , exceptionFile :: FilePath
  , exceptionMsg  :: String
  } deriving (Show, Exception)

readBlockTable
  :: (Floating a, Eq a, DescriptionName n)
  => FilePath
  -> BlockTableKey n Int
  -> IO (BlockTable4d a)
readBlockTable blockTableDir key =
  either (throw . ReadBlockTableException key file) id <$>
  Aeson.eitherDecodeFileStrict' file
  where
    file = blockTableFilePath blockTableDir key
