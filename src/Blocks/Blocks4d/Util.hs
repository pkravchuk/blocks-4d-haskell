{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric  #-}

module Blocks.Blocks4d.Util where

import           Data.Binary  (Binary)
import           GHC.Generics (Generic)

data Parity = Even | Odd
  deriving (Eq, Ord, Show, Generic, Binary)

parity :: Integral a => a -> Parity
parity a | even a    = Even
parity _ | otherwise = Odd
