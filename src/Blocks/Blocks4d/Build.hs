{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DefaultSignatures     #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TupleSections         #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE OverloadedRecordDot   #-}


module Blocks.Blocks4d.Build where

import           Control.Monad.IO.Class               (liftIO)
import           Control.Monad.Reader                 (asks, local)
import           Data.List.NonEmpty                   (groupAllWith)
import           Data.Set                             (Set, singleton, toList)
import qualified Data.Set                             as Set
import           Hyperion                             (Dict (..), Static, cAp,
                                                       cPure, closureDict,
                                                       ptrAp)
import           Hyperion.Bootstrap.Bound.Types    (BoundConfig (..),
                                                       BoundFiles,
                                                       SDPFetchValue, blockDir)
import           Hyperion.Concurrent                  (mapConcurrently_)
import           Hyperion.HasWorkers                  (remoteEval)
import           Hyperion.Job                         (Job, jobNodeCpus,
                                                       setTaskCpus)
import qualified Hyperion.LockMap                     as Lk
import qualified Hyperion.Log                         as Log
import           Hyperion.Util                        (withDict)
import           Hyperion.WorkerCpuPool               (NumCPUs (..))
import qualified Blocks.Blocks4d.ReadTable.Block as B4d
import qualified Blocks.Blocks4d.Types           as B4d
import qualified Blocks.Blocks4d.WriteTable      as B4d
import qualified Blocks.ScalarBlocks             as SB
import           Blocks.ScalarBlocks.Build       (scalarBlockBuildLink)

import           Control.Distributed.Process          (Process)
import           Control.Monad                        (filterM, void)
import           Data.Ratio                           ((%))
import           Data.Semigroup                       (sconcat)
import           Data.Binary                          (Binary)
import           Blocks.Blocks4d.Description     (DescriptionName (..),
                                                       writeDescription)
import           Bootstrap.Build.BuildLink                 (BuildChain,
                                                       BuildLink (..))
import           Bootstrap.Build.FChain                    (FChain (..))
import           System.Directory                     (doesFileExist)
import           System.FilePath.Posix                ((</>))

type instance SDPFetchValue a (B4d.BlockTableKey n Int) = B4d.BlockTable4d a

uncurry5 :: (a -> b -> c -> d -> e -> g) -> ((a, b, c, d, e) -> g)
uncurry5 f ~(a,b,c,d,e) = f a b c d e

-- | Looks for the seeds_4d executable at 'scriptsDir/seeds_4d.sh'
-- where 'scriptsDir' is set by 'BoundConfig'.
seedBuildLink
  :: BoundConfig
  -> BoundFiles
  -> BuildLink Job (B4d.SeedTableKey Int) SB.BlockTableKey
seedBuildLink config files = BuildLink
  { buildDeps = \B4d.SeedTableKey {..} -> [ SB.BlockTableKey
    { dim = 4
    , nmax = ceiling ((lambda+1) % 2) + 2 * spin_p
    , output_ab = True
    , ..
    }]
  , checkCreated = checkCreated' (blockDir files)
  , buildAll = \tables -> do
    let
      -- a bit of magic that collects all keys with the same data except spins and puts sets of the spins inside
      grouped = map (sconcat . fmap (fmap singleton)) $ groupAllWith void tables
    nodeCpus <- asks jobNodeCpus
    local (setTaskCpus nodeCpus) $
      mapConcurrently_ (runSeedBlocks nodeCpus) grouped
  }
  where
    checkCreated' dir = \k -> liftIO (doesFileExist (B4d.seedTableFilePath dir k))
    seeds4dExecutable = scriptsDir config </> "seeds_4d.sh"
    runSeedBlocks (NumCPUs numThreads) t = do
      Log.info "Building seeds" t
      remoteEval $
        (static remoteWriteTables) `ptrAp`
        cPure seeds4dExecutable `cAp`
        cPure numThreads `cAp`
        cPure (blockDir files) `cAp`
        cPure (blockDir files) `cAp`
        cPure t
    remoteWriteTables a1 a2 a3 blockDir t = do
      let
        individualKeys = map (\s -> s <$ t) . toList $ t.spin
      Lk.withLocks (map (,blockDir) individualKeys) $ do
        notcreated <- filterM (fmap not . checkCreated' blockDir) individualKeys
        let
          spin' = mconcat . map (singleton . ((\b -> b.spin) :: B4d.SeedTableKey Int -> Int)) $ notcreated
        if not . Set.null $ spin' then
          liftIO $ B4d.writeSeedBlockTable a1 a2 a3 blockDir t { B4d.spin = spin' }
        else
          return ()

remoteWriteBlock4dTables :: forall n. (DescriptionName n) => (FilePath, Int, FilePath, FilePath, B4d.BlockTableKey n (Set Int))
  -> Process ()
remoteWriteBlock4dTables a@(_,_,_,blockDir,t) = do
  let
    individualKeys = map (\s -> s <$ t) . toList $ t.spin
  Lk.withLocks (map (,blockDir) individualKeys) $ do
    notcreated <- filterM (fmap not . checkCreated' blockDir) individualKeys
    let
      spin' = mconcat . map (singleton . ((\b -> b.spin) :: B4d.BlockTableKey n Int -> Int)) $ notcreated
    if not . Set.null $ spin' then
      liftIO $ maybe (return ()) (
        \descr -> do
        writeDescription blockDir descr
        uncurry5 B4d.writeBlockTable a
        ) (B4d.description t)
    else
      return ()
--Lk.withLock t $ do
--  let
--    individualKeys = map (\s -> s <$ t) . toList $ B4d.spin (t :: B4d.BlockTableKey n (Set Int))
--  alreadyBuilt <- all id <$> sequence (checkCreated' blockDir <$> individualKeys)
--  if not alreadyBuilt then
--    liftIO $ maybe (return ()) (
--      \descr -> do
--      writeDescription blockDir descr
--      uncurry5 B4d.writeBlockTable a
--      ) (B4d.description t)
--  else
--    return ()
  where
    checkCreated' dir = \k -> liftIO (doesFileExist (B4d.blockTableFilePath dir k))

-- deriveDescriptionNameStatics :: TH.Name -> TH.Q [TH.Dec]
-- deriveDescriptionNameStatics name =
--   [d| instance Static (Binary $(TH.conT name)) where
--           closureDict = cPtr (static Dict)
--       instance Static (DescriptionName $(TH.conT name)) where
--           closureDict = cPtr (static Dict)
--   |]

-- | Looks for the blocks_4d executable at 'scriptsDir/blocks_4d.sh',
-- where 'scriptsDir' is set in 'BoundConfig'
block4dBuildLink
  :: forall n. (Static (DescriptionName n), Static (Binary n))
  => BoundConfig
  -> BoundFiles
  -> BuildLink Job (B4d.BlockTableKey n Int) (B4d.SeedTableKey Int)
block4dBuildLink config files = BuildLink
  { buildDeps = \t -> map (fmap (const t.spin)) $ B4d.dependencies t
  , checkCreated = \k -> liftIO (doesFileExist (B4d.blockTableFilePath (blockDir files) k))
  , buildAll = \tables -> do
    let
      -- a bit of magic that collects all keys with the same data except spins and puts sets of the spins inside
      grouped = map (sconcat . fmap (fmap singleton)) $ groupAllWith void tables
    nodeCpus <- asks jobNodeCpus
    local (setTaskCpus nodeCpus) $
      mapConcurrently_ (runBlocks nodeCpus) grouped
  }
  where
    blocks4dExecutable = scriptsDir config </> "blocks_4d.sh"
    runBlocks (NumCPUs numThreads) t = do
      Log.info "Building blocks" t
      remoteEval $ static (withDict remoteWriteBlock4dTables :: Dict (DescriptionName n) -> (FilePath, Int, FilePath, FilePath, B4d.BlockTableKey n (Set Int))
                                                            -> Process ()) `ptrAp` closureDict `cAp`
                                                            cPure (blocks4dExecutable, numThreads, (blockDir files), (blockDir files), t)

block4dBuildChain
  :: forall n. (Static (DescriptionName n), Static (Binary n))
  => BoundConfig
  -> BoundFiles
  -> BuildChain Job '[(B4d.BlockTableKey n Int), (B4d.SeedTableKey Int), SB.BlockTableKey]
block4dBuildChain config files =
  block4dBuildLink config files :<
  seedBuildLink config files    :<
  FChainEnd (scalarBlockBuildLink config files True)
