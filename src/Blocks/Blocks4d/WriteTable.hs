{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE MultiWayIf            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE OverloadedRecordDot   #-}

module Blocks.Blocks4d.WriteTable where

import           Control.Monad                    (void)
import           Data.Aeson                       (encode)
import           Data.BinaryHash                  (hashBase64Safe)
import           Data.ByteString.Lazy.Char8       (unpack)
import           Data.List                        (intercalate)
import           Data.Rendered                    (render)
import qualified Data.Set                         as Set
import           Blocks.Blocks4d.Description (DescriptionName,
                                                   descriptionFileName)
import           Blocks.Blocks4d.Types       (BlockTableKey (..),
                                                   SeedTableKey (..))
import qualified Blocks.ScalarBlocks         as SB
import           System.Exit                      (ExitCode (..))
import           System.FilePath                  (takeDirectory, (<.>), (</>))
import           System.Process                   (readProcessWithExitCode,
                                                   showCommandForUser)

renderRational :: Int -> Rational -> String
renderRational prec x =
  (if x >= 0 then "" else "-") ++
  (if integerPart == "" then "0" else integerPart) ++
  (if prec > 0 then "." else "") ++
  replicate (prec - length decimalPart) '0' ++
  decimalPart
  where
    integerRep :: Integer
    integerRep = round (abs x * 10^prec)

    integerStr = show integerRep

    integerPart = take (length integerStr - prec) integerStr
    decimalPart = reverse . take prec . reverse $ integerStr

-- | The path for a block table takes the form
-- blocks_4d_tables_hash/spin_j.json, where "hash" is the hash of
-- BlockTableKey with jInternal = (), and "j" is the value of
-- jInternal for that table (filled in by blocks_4d). Using the hash
-- has two disadvantages: (1) There can be collisions. This would be
-- very bad. Currently Hashable targets Int64, so the probability of a
-- collision is pretty small if we have a few thousand block
-- files. (2) The filename doesn't look sensible to humans. However,
-- each file contains its own metadata, so this can be overcome with
-- tooling.
--
-- TODO: Get rid of Hashable!
mkBlockTableFilePath :: (DescriptionName n) => FilePath -> BlockTableKey n () -> String -> FilePath
mkBlockTableFilePath blockTableDir blockTable spinString =
  blockTableDir </> tableName </> "spin_" ++ spinString <.> "json"
  where
    tableName = "blocks_4d_tables_" ++ hashBase64Safe blockTable

-- | A file template for passing to blocks_4d
blockTableFileTemplate :: (DescriptionName n) => FilePath -> BlockTableKey n j -> FilePath
blockTableFileTemplate blockTableDir blockTable =
  mkBlockTableFilePath blockTableDir (void blockTable) "{}"

-- | The file corresponding to a specific value of jInternal
blockTableFilePath :: forall n. (DescriptionName n) => FilePath -> BlockTableKey n Int -> FilePath
blockTableFilePath blockTableDir blockTable =
  mkBlockTableFilePath blockTableDir (void blockTable) $ show blockTable.spin

seedTableFilePath_ :: FilePath -> SeedTableKey Int -> FilePath
seedTableFilePath_ blockTableDir SeedTableKey {..} =
  blockTableDir </> -- "lambda" ++ show lambda </>
  concat ("seedBlockTable4D" : kvs)
  <.> "m"
  where
    kvs = [ "-" ++ k ++ v | (k, v) <- blockTableStrings ]
    blockTableStrings =
      [ ("type-",          show seedType)
      , ("perm-",          show permutation)
      , ("P",              show spin_p)
      , ("L",              show spin)
      , ("delta12-",       render delta12)
      , ("delta34-",       render delta34)
      , ("lambda-",        show lambda)
      , ("keptPoleOrder-", show keptPoleOrder)
      , ("order-",         show order)
      ]

seedTableFilePath :: FilePath -> SeedTableKey Int -> FilePath
seedTableFilePath blockTableDir k@SeedTableKey {..} =
  if | spin_p > 0  -> seedTableFilePath_ blockTableDir k
     | spin_p == 0 -> SB.blockTableFilePath blockTableDir $
        SB.BlockTableKey { dim = 4, nmax = - (- lambda - 1) `div` 2, output_ab = True, ..}
     | otherwise   -> error "Negaive p in SeedTableKey"

writeSeedTableArgs :: Int -> FilePath -> FilePath -> SeedTableKey (Set.Set Int) -> [String]
writeSeedTableArgs numThreads scalarDir blockTableDir k@SeedTableKey{..} =
  [ "--perm"       , show permutation
  , "--type"       , show seedType
  , "--p"          , show spin_p
  , "--delta-12"   , render delta12
  , "--delta-34"   , render delta34
  , "--threads"    , show numThreads
  , "--precision"  , show precision
  , "--order"      , show order
  , "--poles"      , show keptPoleOrder
  , "--spin-ranges", showSpins
  , "--lambda"     , show lambda
  , "-i"           , scalarDir
  , "-o"           , outputDir
  ]
  where
    showSpins = intercalate "," . map show . Set.toList $ spin
    outputDir = takeDirectory (seedTableFilePath blockTableDir (fmap (const 0) k))

writeBlockTableArgs :: (DescriptionName n) => Int -> FilePath -> FilePath -> BlockTableKey n (Set.Set Int) -> Maybe [String]
writeBlockTableArgs numThreads seedDir blockTableDir k@BlockTableKey{..} = (\descr ->
  [ "-d"               , descriptionFileName blockTableDir descr
  , "-p"               , unpack . encode $ fmap render parameters
  , "--float-format"   , show floatFormat
  , "--threads"        , show numThreads
  , "--precision"      , show precision
  , "--order"          , show order
  , "--poles"          , show keptPoleOrder
  , "--finalize_poles" , show keptPoleOrder_final
  , "--spin-ranges"    , showSpins
  , "--lambda"         , show lambda
  , "-i"               , seedDir
  , "-o"               , blockTableFileTemplate blockTableDir k
  ]) <$> description
  where
    showSpins = intercalate "," . map show . Set.toList $ spin

writeSeedBlockTable
  :: FilePath
  -> Int
  -> FilePath
  -> FilePath
  -> SeedTableKey (Set.Set Int)
  -> IO ()
writeSeedBlockTable seedBlocksExecutable numThreads scalarDir blockTableDir b = do
  (exitCode, stdout, stderr) <-
    readProcessWithExitCode seedBlocksExecutable args ""
  case exitCode of
    ExitSuccess -> return ()
    _ -> error $ showCommandForUser seedBlocksExecutable args ++ " failed; stdout: " ++ stdout ++ "; stderr: " ++ stderr
  where
    args = writeSeedTableArgs numThreads scalarDir blockTableDir b

writeBlockTable
  :: (DescriptionName n)
  => FilePath
  -> Int
  -> FilePath
  -> FilePath
  -> BlockTableKey n (Set.Set Int)
  -> IO ()
writeBlockTable blocks4DExecutable numThreads seedDir blockTableDir b = flip (maybe (return ())) margs $ \args -> do
  (exitCode, stdout, stderr) <-
    readProcessWithExitCode blocks4DExecutable args ""
  case exitCode of
    ExitSuccess -> return ()
    _ -> error $ showCommandForUser blocks4DExecutable args ++ " failed; stdout: " ++ stdout ++ "; stderr: " ++ stderr
  where
    margs = writeBlockTableArgs numThreads seedDir blockTableDir b
